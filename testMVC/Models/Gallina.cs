﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace testMVC.Models
{
    public class Gallina : BaseModel
    {
        

        public int Id { get; set; }
        public string Nom { get; set; }
        public string Tipus { get; set; }
        public int Any { get; set; }

        public Gallina(int id,  string nom, string tipus, int any)
        {
            this.Id = id;
            this.Nom = nom;
            this.Tipus = tipus;
            this.Any = any;
        }

        public Gallina() { }

        public override string GetTable()
        {
            return "gallines";

        }
    }
}
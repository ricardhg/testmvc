﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using testMVC.Models;
using MySql.Data;
using MySql.Data.MySqlClient;


namespace testMVC.Controllers
{
    public abstract class BaseController : Controller
    {
        public static MySqlConnection _conn;
        public BaseModel _model;
        

        static BaseController()
        {
            // string connStr = "server=localhost;user=root;database=webtest;port=3306;password=secret";
            string connStr = "server=localhost;user=root;database=webtest;port=3306";
            _conn = new MySqlConnection(connStr);
        }


        public abstract Object ToModel(MySqlDataReader rdr);
        public abstract Object ToModel(FormCollection collection);
        public abstract void SetUpdateCommand(MySqlCommand cmd, Object ob, int id);
        public abstract void SetCreateCommand(MySqlCommand cmd, Object ob);

        public void UpdateId(int id, FormCollection collection)
        {
            try
            {
                if (_conn.State == System.Data.ConnectionState.Closed)
                {
                    _conn.Open();
                }
                Object obj = this.ToModel(collection);

                MySqlCommand cmd = new MySqlCommand();
                this.SetUpdateCommand(cmd, obj, id);
                cmd.Connection = _conn;
                cmd.Prepare();
                int rows_changed = cmd.ExecuteNonQuery();

                _conn.Close();
            } catch (Exception e)
            {
                if (_conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
                throw e;
            }
            
        }

        public void DeleteId(int id)
        {
            try
            {
                if (_conn.State == System.Data.ConnectionState.Closed)
                {
                    _conn.Open();
                }
             
                MySqlCommand cmd = new MySqlCommand();
                string sql = string.Format("DELETE FROM {0} WHERE id={1}", _model.GetTable(), id);

                cmd.CommandText = sql;
                cmd.Connection = _conn;
                cmd.Prepare();
                int rows_changed = cmd.ExecuteNonQuery();

                _conn.Close();
            }
            catch (Exception e)
            {
                if (_conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
                throw e;
            }

        }

        public void CreateItem(FormCollection collection)
        {
            try
            {
                if (_conn.State == System.Data.ConnectionState.Closed)
                {
                    _conn.Open();
                }
                Object obj = this.ToModel(collection);

                MySqlCommand cmd = new MySqlCommand();
                this.SetCreateCommand(cmd, obj);
                cmd.Connection = _conn;
                cmd.Prepare();
                int rows_changed = cmd.ExecuteNonQuery();

                _conn.Close();
            }
            catch (Exception e)
            {
                if (_conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
                throw e;
            }

        }

        public Object GetId(int id)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            Console.WriteLine("buscant id {0}", id);
            try
            {
                string sql = string.Format("SELECT * FROM {0} WHERE id={1}", _model.GetTable(), id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                Object ob = null;
                if (rdr.Read()) ob = this.ToModel(rdr);
                rdr.Close();
                _conn.Close();
                return ob;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
        }

        public List<Object> GetAll()
        {
            _conn.Open();

            try
            {
                string sql = string.Format("SELECT * FROM {0} limit 100", _model.GetTable());
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                List<Object> llista = new List<Object>();
                while (rdr.Read())
                {
                    llista.Add(this.ToModel(rdr));
                }
                rdr.Close();
                _conn.Close();
                return llista;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }

        }


        // //////////////////////////////////////////////////////////////////////////////////////////////
        // //////////////////////////////////////////////////////////////////////////////////////////////
        // //////////////////////////////////////////////////////////////////////////////////////////////
        // //////////////////////////////////////////////////////////////////////////////////////////////
        // //////////////////////////////////////////////////////////////////////////////////////////////
        // //////////////////////////////////////////////////////////////////////////////////////////////
        // //////////////////////////////////////////////////////////////////////////////////////////////
        // //////////////////////////////////////////////////////////////////////////////////////////////
        // //////////////////////////////////////////////////////////////////////////////////////////////


       

        // GET: Gallina/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Gallina/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                this.CreateItem(collection);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Gallina/Edit/5
        public ActionResult Edit(int id)
        {
            return View((Gallina)this.GetId(id));
        }

        // POST: Gallina/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                this.UpdateId(id, collection);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }
        }

        // POST: Gallina/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this.DeleteId(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }




    }
}
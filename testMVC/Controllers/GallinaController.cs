﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using testMVC.Models;
using MySql.Data;
using MySql.Data.MySqlClient;


namespace testMVC.Controllers
{
    public class GallinaController : BaseController
    {

        public GallinaController() : base()
        {
            this._model = new Gallina();

        }
    
        public override Object ToModel(MySqlDataReader rdr)
        {
            Gallina Model = new Gallina();
            
            Model.Id = Convert.ToInt32(rdr["id"]);
            Model.Nom = rdr["nom"].ToString();
            Model.Tipus = rdr["tipus"].ToString();
            Model.Any = Convert.ToInt32(rdr["any"]);
         
            return Model;
        }

        public override Object ToModel(FormCollection collection)
        {
            int _id = 0;
            int _any = 0;

            Gallina Model = new Gallina();
            Int32.TryParse(collection["id"], out _id);
            Int32.TryParse(collection["any"], out _any);

            Model.Id = _id;
            Model.Nom = collection["nom"].ToString();
            Model.Tipus = collection["tipus"].ToString();
            Model.Any = _any;
          
            return Model;
        }

        public override void SetUpdateCommand(MySqlCommand cmd, Object obj, int id)
        {

            Gallina g = (Gallina)obj;

            string sql = string.Format("UPDATE {0} SET nom=@nom, tipus=@tipus, any=@any where id={1}", _model.GetTable(), id);
            
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@nom", g.Nom);
            cmd.Parameters.AddWithValue("@tipus", g.Tipus);
            cmd.Parameters.AddWithValue("@any", g.Any);
            
        }

        public override void SetCreateCommand(MySqlCommand cmd, Object obj)
        {

            Gallina g = (Gallina)obj;

            string sql = string.Format("INSERT INTO {0} (nom, tipus, any) VALUES (@nom,@tipus,@any)", _model.GetTable());

            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@nom", g.Nom);
            cmd.Parameters.AddWithValue("@tipus", g.Tipus);
            cmd.Parameters.AddWithValue("@any", g.Any);

        }

        // GET: Gallina
        public ActionResult Index()
        {
            return View(this.GetModelAll());
        }

        
        public List<Gallina> GetModelAll()
        {
            return this.GetAll().Cast<Gallina>().ToList();
        }

/*

        public Object GetModelIdxx(int id)
        {
            return Convert.ChangeType(this.GetId(id), this._model.GetType());
        }
*/

        public Gallina GetModelId(int id)
        {
            return (Gallina)this.GetId(id);
        }


        // GET: Gallina/Details/5
        public ActionResult Details(int id)
        {
            return View(this.GetModelId(id));
        }


        // GET: Gallina/Delete/5
        public ActionResult Delete(int id)
        {
            return View(this.GetModelId(id));
        }

    }
}
